import express from "express";
import { client } from "../main";
const bookmarkPageRoute = express.Router();
// bookmarkPageRoute.get("/bookmark/:id", async (req, res) => {
// const bookmarkId = req.params.id;
//   let bookmarkIdRow = await client.query(`
// select * from bookmark join product on product.users_id = bookmark.users_id where bookmark.users_id = ${bookmarkId}
// `);
//   let bkId = bookmarkIdRow[0].id;

//   try {
//     if (bkUserId) {
//       const isBookmarked =
//         (
//           await client.query(
//             `select * from group_purchases where users_id = ${bookmarkId} `,
//             [bkId, bkUserId]
//           )
//         ).rowCount > 0;
//       if (isBookmarked) {
//         await client.query(
//           `delete from group_purchases where users_id = ${bookmarkId}`,
//           [bkId, bkUserId]
//         );
//         res.json({ success: true });
//         return;
//       }
//     }
//   } catch (error) {
//     console.log(error);
//   }
// });

bookmarkPageRoute.get("/bookmark", async (req, res) => {
    let bkUserId = req.session["user"].id;
    let bookmarkResult = await client.query(`
  select p.name, p.price, u.username, p.image, p.id as product_id from bookmark b
  inner join group_purchases gp on gp.id = b.group_purchases_id 
  inner join product p on p.id = gp.product_id 
  inner join users u on u.id  = p.users_id 
where b.users_id = ${bkUserId}
`);
    let bookmarkData = bookmarkResult.rows;
    res.json(bookmarkData);
});

export default bookmarkPageRoute;
