import pg from "pg";
import dotenv from "dotenv";
import xlsx from "xlsx";
import { hashPassword } from "../hash";
dotenv.config();

console.log("database:", process.env.DB_NAME);

console.log("database:", process.env.DB_USERNAME, process.env.DB_PASSWORD);

interface User {
  icon: string;
  username: string;
  email: string;
  password: string;
}
interface Product {
  image: string;
  users_id: number;
  qtn: number;
  description: string;
  district: string;
  name: string;
  price: number;
}
interface GroupPurchase {
  users_id: number;
  product_id: number;
  status: string;
  expiry: Date;
  categories_id: number;
}
interface Categories {
  name: string;
}
interface Bookmark {
  users_id: number;
  group_purchase_id: number;
}
interface JoinPurchase {
  group_purchase_id: number;
  users_id: number;
}

let wb = xlsx.readFile("init_data.xlsx");
const userSheets = wb.Sheets["user"];
const users: User[] = xlsx.utils.sheet_to_json(userSheets);
const productSheets = wb.Sheets["product"];
const products: Product[] = xlsx.utils.sheet_to_json(productSheets);
const purchaseSheets = wb.Sheets["group_purchase"];
const purchases: GroupPurchase[] = xlsx.utils.sheet_to_json(purchaseSheets);
const categoriesSheets = wb.Sheets["categories"];
const categories: Categories[] = xlsx.utils.sheet_to_json(categoriesSheets);
const bookmarkSheets = wb.Sheets["bookmark"];
const bookmarks: Bookmark[] = xlsx.utils.sheet_to_json(bookmarkSheets);
const joinSheets = wb.Sheets["join_purchases"];
const joins: JoinPurchase[] = xlsx.utils.sheet_to_json(joinSheets);

const client = new pg.Client({
  database: process.env.DB_NAME,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
});

async function main() {
  await client.connect();

  await client.query("delete from bookmark");

  await client.query(`delete from "join_purchase"`);

  await client.query(`delete from "group_purchases"`);

  await client.query("delete from product");

  await client.query("delete from categories");

  await client.query("delete from users");

  let insertUserId: any[] = [];
  for (let user of users) {
    let originalPassword = user.password;
    console.log("originalPassword = ", originalPassword);

    let hashedPassword = await hashPassword(originalPassword.toString());
    console.log("hashedPassword = ", hashedPassword);

    let insertUserResult = await client.query(
      "INSERT INTO users (icon,username,email,password, created_at, updated_at) values ($1,$2,$3,$4, NOW(), NOW()) returning id",
      [user.icon, user.username, user.email, hashedPassword]
    );
    insertUserId.push(insertUserResult.rows[0].id);
  }
  console.table(insertUserId);

  let insertProductId: any[] = [];
  for (let productIndex = 0; productIndex < products.length; productIndex++) {
    const product = products[productIndex];
    let insertProductResult = await client.query(
      "INSERT INTO product (image,users_id,qtn,description,district,name,price, created_at, updated_at) values ($1,$2,$3,$4,$5,$6,$7, NOW(), NOW()) returning id",
      [
        product.image,
        insertUserId[productIndex % insertUserId.length],
        product.qtn,
        product.description,
        product.district,
        product.name,
        product.price,
      ]
    );
    insertProductId.push(insertProductResult.rows[0].id);
  }

  let insertCategoriesId: any[] = [];
  for (let categorie of categories) {
    console.log({ categorie });

    let insertCategoriesResult = await client.query(
      "INSERT INTO categories (name, created_at, updated_at) values ($1, NOW(), NOW()) returning id",
      [categorie.name]
    );
    insertCategoriesId.push(insertCategoriesResult.rows[0].id);
  }
  let insertPurchaseId: any[] = [];
  for (
    let purchaseIndex = 0;
    purchaseIndex < purchases.length;
    purchaseIndex++
  ) {
    const purchase = purchases[purchaseIndex];
    let insertPurchaseResult = await client.query(
      "INSERT INTO group_purchases (users_id,product_id,status,expiry_date,categories_id, created_at, updated_at) values ($1,$2,$3,$4,$5, NOW(), NOW()) returning id",
      [
        insertUserId[purchaseIndex % insertUserId.length],
        insertProductId[purchaseIndex % insertProductId.length],
        purchase.status,
        purchase.expiry,
        insertCategoriesId[purchaseIndex % insertCategoriesId.length],
      ]
    );
    insertPurchaseId.push(insertPurchaseResult.rows[0].id);
  }
  for (let joinIndex = 0; joinIndex < joins.length; joinIndex++) {
    // const join = joins[joinIndex];
    await client.query(
      "INSERT INTO join_purchase (group_purchases_id,users_id, created_at, updated_at) values ($1,$2, NOW(), NOW())",
      [
        insertPurchaseId[joinIndex % insertPurchaseId.length],
        insertUserId[joinIndex % insertUserId.length],
      ]
    );
  }
  for (
    let bookmarkIndex = 0;
    bookmarkIndex < bookmarks.length;
    bookmarkIndex++
  ) {
    await client.query(
      "INSERT INTO bookmark (users_id,group_purchases_id, created_at, updated_at) values ($1,$2, NOW(), NOW())",
      [
        insertUserId[bookmarkIndex % insertUserId.length],
        insertPurchaseId[bookmarkIndex % insertPurchaseId.length],
      ]
    );
  }

  const userCount = await client.query(`select * from users`);
  const joinCount = await client.query(`select * from "bookmark"`);
  console.log(`user: ${userCount.rows.length}`);
  console.log(`joinPurchase: ${joinCount.rows.length}`);
  await client.end();
}
main();
