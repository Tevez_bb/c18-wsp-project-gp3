// $(document).ready(function(){
//   $(".owl-carousel").owlCarousel();
// });

async function fetchUser() {
  let res = await fetch("/profile");
  if (res.ok) {
    let result = await res.json();
    let resultData = result.data;
    document.querySelector(".avatar").innerHTML = "";
    document.querySelector(".avatar").removeAttribute("hidden");
    document.querySelector(".btn-logout").removeAttribute("hidden");
    document.querySelector(".btn-login").setAttribute("hidden", "");
    document.querySelector(".wishlist").removeAttribute("hidden");
    // console.log(resultData.user.icon);

    if (resultData.user.icon) {
      document.querySelector(
        ".avatar"
      ).innerHTML += `<a href="/admin/profile.html">
      <img src="${resultData.user.icon}" class="avatar-style"></img>
      </a>`;
    } else if (resultData.user.icon == null) {
      // console.log("print icon la");
      document.querySelector(
        ".avatar"
      ).innerHTML += `<a href="/admin/profile.html">
      <img src="./assets/default-avatar.jpeg" class="avatar-style"></img>
      </a>`;
    }
  } else {
    document.querySelector(".btn-login").removeAttribute("hidden");
    document.querySelector(".avatar").setAttribute("hidden", "");
    document.querySelector(".btn-logout").setAttribute("hidden", "");
    document.querySelector(".wishlist").setAttribute("hidden", "");
  }
}
fetchUser();

document.querySelector(".btn-login").addEventListener("click", function () {
  window.location.href = "login.html";
});
document
  .querySelector(".btn-create-deal")
  .addEventListener("click", function () {
    window.location.href = "admin/product_reg.html";
  });
document.querySelector(".btn-logout").addEventListener("click", async (e) => {
  e.preventDefault();
  // let logoutElem = e.currentTarget;
  let res = await fetch("/logout", {
    method: "POST",
  });
  if (res.ok) {
    console.log("logged out");
    let result = await res.json();
    document.querySelector(".btn-login").removeAttribute("hidden");
    document.querySelector(".avatar").setAttribute("hidden", "");
    document.querySelector(".btn-logout").setAttribute("hidden", "");
    document.querySelector(".wishlist").setAttribute("hidden", "");
    window.location.href = "/";
  }
});
document.querySelector(".header-logo").addEventListener("click", function () {
  window.location.href = "/";
});
