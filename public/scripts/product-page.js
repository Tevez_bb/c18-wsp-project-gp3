let params = new URLSearchParams(document.location.search);
let productId = params.get("productId");
console.log(`productID js: `, productId);

async function fetchProductDetail() {
    // console.log("hello");
    let res = await fetch(`/product-detail/${productId}`);
    let productDetailList = await res.json();

    console.log(`productDetailList:`, productDetailList);
    const productDetailData = productDetailList.productData;
    const productDetailCount = productDetailList.joinCountData;
    // const productOwner = productDetailList.isOwner;
    const relatedItems = productDetailList.relatedItemData;
    console.log(`productDetailData: `, productDetailData);
    console.log(`relatedItems:`, relatedItems);
    console.log("productDetailCount:", productDetailCount[0]["count"]);
    // console.log(`productOwner =`, productOwner);
    let productDetailContainerElem = document.querySelector("#product-detail-container");
    // console.log(products);
    let newProductString = getProductDetailHTMLString(productDetailData, productDetailCount);
    productDetailContainerElem.innerHTML = newProductString;

    let relatedItemsContainer = document.querySelector("#related-item-container");
    for (let relatedItem of relatedItems.slice(0, 4)) {
        let newItemString = getRelatedItemHTMLString(relatedItem);
        relatedItemsContainer.innerHTML += newItemString;
    }
}

function getRelatedItemHTMLString(relatedItem) {
    let relatedItemsHTMLString = `
  <div class="col">
  <div class="card h-100 product-card">
  <a href="/product.html?productId=${relatedItem.productid}">
      <img src="${relatedItem.image}" class="card-img-top my-img">
      </a>
      <div class="card-body">
          <h2>${relatedItem.name}</h2>
          <p class="seller-name">${relatedItem.username}</p>
          <p class="product-price">${relatedItem.price}</p>
      </div>
  </div>
</div>
    `;
    return relatedItemsHTMLString;
}

function getProductDetailHTMLString(productDetailData, productDetailCount) {
    // console.log(productDetailData);
    let productDetailHTMLString = `
 
    <div class="row">
      <div class="col-lg-6">
        <div class="product-category">${productDetailData.category}</div>
        <img src="${productDetailData.image}" class="product-image-big"></img>
      </div>
      <div class="col-lg-6 product-detail-container">
        <div>
            <h1 class="product-name">${productDetailData.name}</h1>
            <div class="product-price">HK${productDetailData.price}</div>
            <div class="seller">Seller: ${productDetailData.username}</div>
            <div class="district">District: ${productDetailData.district}</div>
            <div class="end-date">Campaign Ends Date: ${convertToLocalDate(productDetailData.expiry_date)}</div>
            <div class="participant">No. of participants joined: ${productDetailCount[0]["count"]}</div>
            <div class="qtn">Available quantity: ${productDetailData.qtn}</div>
          </div>
          <div class="row">
            <div class="flex-button">
              <button type="button" class="btn btn-submit-green join-button" onclick="joinBuy()"><i class="bi bi-cart"></i>  Order</button>
              <button type="button" hidden class="btn btn-submit-green cancel-button" onclick="removeBuy()"><i class="bi bi-x"></i> Cancel Order</button>
              <button type="button" class="btn btn-outline btn-wishlist" onclick="bookmarkBuy()"><span class="btn-like"><i class="bi bi-suit-heart-fill"></i></span>  Add to Wishlist</button>
              <button type="button" hidden class="btn btn-outline btn-liked" onclick="removeBookmark()"><span class="btn-liked"><i class="bi bi-suit-heart-fill"></i></span>  Remove from Wishlist</button>
            </div> 
            <div class="result"></div> 
          </div>
      </div>
    </div>
    <div>
    <div class="title-name">Product Description</div>
      <p class="description">${productDetailData.description}</p>
    </div>
</div>
        `;
    return productDetailHTMLString;
}

async function fetchUser() {
    let res = await fetch("/profile");
    if (res.ok) {
        let result = await res.json();
        let resultData = result.data;
        document.querySelector(".avatar").innerHTML = "";
        document.querySelector(".avatar").removeAttribute("hidden");
        document.querySelector(".btn-logout").removeAttribute("hidden");
        document.querySelector(".btn-login").setAttribute("hidden", "");

        // console.log(resultData.user.icon);

        if (resultData.user.icon) {
            document.querySelector(".avatar").innerHTML += `<img src="${resultData.user.icon}" class="avatar-style"></img>`;
        } else if (resultData.user.icon == "null") {
            document.querySelector(".avatar").innerHTML += `<img src="./assets/default-avatar.jpeg" class="avatar-style"></img>`;
        }
    } else {
        document.querySelector(".btn-login").removeAttribute("hidden");
        document.querySelector(".avatar").setAttribute("hidden", "");
        document.querySelector(".btn-logout").setAttribute("hidden", "");
    }
}

document.querySelector(".btn-logout").addEventListener("click", async (e) => {
    e.preventDefault();
    // let logoutElem = e.currentTarget;
    let res = await fetch("/logout", {
        method: "POST",
    });
    if (res.ok) {
        console.log("logged out");
        let result = await res.json();
        document.querySelector(".btn-login").removeAttribute("hidden");
        document.querySelector(".avatar").setAttribute("hidden", "");
        document.querySelector(".btn-logout").setAttribute("hidden", "", function () {
            window.location.href = "/";
        });
    }
});
//

document.querySelector(".btn-login").addEventListener("click", function () {
    window.location.href = "login.html";
});
document.querySelector(".btn-create-deal").addEventListener("click", function () {
    window.location.href = "admin/product_reg.html";
});
document.querySelector(".header-logo").addEventListener("click", function () {
    window.location.href = "/";
});

async function joinBuy() {
    let res = await fetch(`join/${productId}`, {
        method: "put",
    });
    const joinList = await res.json();
    console.log(joinList);
    document.querySelector(".result").innerText = joinList.message;
    document.querySelector(".join-button").setAttribute("hidden", "");
    document.querySelector(".cancel-button").removeAttribute("hidden");
}

async function hasJoinedChecking() {
    let res = await fetch(`join/${productId}`);
    const checkingResult = await res.json();
    console.log(`checkingResult:`, checkingResult);
    if (checkingResult.status == true) {
        document.querySelector(".join-button").setAttribute("hidden", "");
        document.querySelector(".cancel-button").removeAttribute("hidden");
    } else {
        document.querySelector(".join-button").removeAttribute("hidden");
        document.querySelector(".cancel-button").setAttribute("hidden", "");
    }
}

async function removeBuy() {
    let res = await fetch(`join/${productId}`, {
        method: "put",
    });
    const joinList = await res.json();
    document.querySelector(".result").innerText = joinList.message;
    console.log(`joinList: `, joinList);
    document.querySelector(".join-button").removeAttribute("hidden");
    document.querySelector(".cancel-button").setAttribute("hidden", "");
}

async function bookmarkBuy() {
    let res = await fetch(`bookmark/${productId}`, {
        method: "put",
    });
    const bookmarkList = await res.json();
    console.log(bookmarkList);
    document.querySelector(".result").innerText = bookmarkList.message;
    document.querySelector(".btn-wishlist").setAttribute("hidden", "");
    document.querySelector(".btn-liked").removeAttribute("hidden");
}

async function hasBookmarkedChecking() {
    let res = await fetch(`bookmark/${productId}`);
    const checkBookmarkResult = await res.json();
    console.log(`checkBookmarkResult: `, checkBookmarkResult);
    setTimeout(() => {
        if (checkBookmarkResult.status == true) {
            document.querySelector(".btn-wishlist").setAttribute("hidden", "");
            document.querySelector(".btn-liked").removeAttribute("hidden");
        } else {
            document.querySelector(".btn-wishlist").removeAttribute("hidden");
            document.querySelector(".btn-liked").setAttribute("hidden", "");
        }
    }, 100);
}

async function removeBookmark() {
    let res = await fetch(`bookmark/${productId}`, {
        method: "put",
    });
    const bookmarkList = await res.json();
    document.querySelector(".result").innerText = bookmarkList.message;
    console.log(`bookmarkList:`, bookmarkList);
    document.querySelector(".btn-wishlist").removeAttribute("hidden");
    document.querySelector(".btn-liked").setAttribute("hidden", "");
}

// local time zone
function convertToLocalDate(timeString) {
    let date = new Date(timeString).toLocaleDateString();
    if (date.includes("/")) {
        date = date.split("/").reverse().join("-");
    }
    return date;
}

function convertToLocalTime(timeString) {
    let date = new Date(timeString).toLocaleTimeString();
    if (date.includes("/")) {
        date = date.split("/").reverse().join("-");
    }
    // let time = new Date(timeString).toLocaleTimeString();
    return date;
}

async function init() {
    await fetchProductDetail();
    fetchUser();
    hasJoinedChecking();
    hasBookmarkedChecking();
}

init();
