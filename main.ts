import express from "express";
import expressSession from "express-session";
import multer from "multer";
import path from "path";
import pg from "pg";
import grant from "grant";
import { hashPassword, checkPassword } from "./hash";
import dotenv from "dotenv";
import productRegRoutes from "./routes/productRegRoutes";
import userRoutes from "./routes/userRoutes";
import productPageRoute from "./routes/productPageRoute";
import catalogRoute from "./routes/catalog";
import bookmarkPageRoute from "./routes/bookmarkPageRoute";
import profilePageRoute from "./routes/profilePageRoutes";
import productEditRoute from "./routes/productEditRoute";
// import {Request,Response} from 'express';
import http from "http";
import { Server as SocketIO } from "socket.io";

const app = express();
const server = new http.Server(app);
const io = new SocketIO(server);

dotenv.config();

const grantExpress = grant.express({
    defaults: {
        origin: "http://localhost:8888",
        transport: "session",
        state: true,
    },
    google: {
        key: process.env.GOOGLE_CLIENT_ID || "",
        secret: process.env.GOOGLE_CLIENT_SECRET || "",
        scope: ["profile", "email"],
        callback: "/login/google",
    },
});

export const client = new pg.Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
});

client.connect();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./uploads"));
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
    },
});

export const upload = multer({ storage });
// 教express 識睇 urlencoded 既req.body
app.use(express.urlencoded({ extended: true }));

// 教express 識睇 JSON 既req.body
app.use(express.json());

const sessionMiddleware = expressSession({
    secret: "so sleepy",
    resave: true,
    saveUninitialized: true,
});
app.use(sessionMiddleware);
io.use((socket, next) => {
    let req = socket.request as express.Request;
    let res = req.res as express.Response;
    sessionMiddleware(req, res, next as express.NextFunction);
});
//chat room
io.on("connection", (socket) => {
    // socket.req.session
    let req = socket.request as express.Request;

    if (req.session && req.session["user"]) {
        console.log("session found :", socket.request["session"]["user"].username);
    } else {
        console.log("user is not found in io connction");
        let dateString = new Date();
        req.session["user"] = {
            username: `User_${dateString.toISOString()}`,
        };
    }
    socket.request["session"].save();
    // console.log("You are logged in as :", req.session["user"]);

    console.log("socket id = ", socket.id);

    socket.on("join-chatroom", (id) => {
        console.log(`${req.session["user"].username} joined chatroom ${id}`);
        socket.join("chatroom");
    });
    socket.on("chat message", (msg) => {
        io.to("chatroom").emit("chat message", msg);
    });
});

app.post("/message", (req, res) => {
    let incommingMsg = req.body.message;
    let username = req.session["user"].username;
    console.log(`${username} says : ${incommingMsg}`);
    let message = {
        content: incommingMsg,
        from: username,
    };
    io.emit("chat message", message);
    res.json();
});

app.get("/products", async (req, res) => {
    // let productResult = await client.query(`
    // select u.id, p.name, p.price, gp.expiry_date, gp.categories_id as category_id, p.district, p.qtn, p.image, u.username, p.description, c.name as category from product p
    // inner join users u
    // 	on u.id = p.users_id
    // inner join group_purchases gp
    // 	on gp.product_id = p.id
    // inner join categories c
    // 	on c.id = gp.categories_id`);

    let productResult = await client.query(`
    select u.username, p.name, p.price, p.district, p.id as productid, p.image from product p inner join users u on u.id = p.users_id`);

    // console.log(productResult.rows);
    let productData = productResult.rows;
    res.json(productData);
});

// socket.io <start>

app.use(sessionMiddleware);

io.use((socket, next) => {
    let req = socket.request as express.Request;
    let res = req.res as express.Response;
    sessionMiddleware(req, res, next as express.NextFunction);
});
//...

app.get("/onlineUser", async (req, res) => {
    let onlineUserResult = await client.query(`
  select u.username, p.name, p.price, p.district from product p inner join users u on u.id = p.users_id`);
    let userData = onlineUserResult.rowCount;
    res.json(userData);
});
// socket.io <end>
app.use(grantExpress as express.RequestHandler);
app.use(userRoutes);
app.use(productRegRoutes);
app.use(productPageRoute);
app.use(catalogRoute);
app.use(bookmarkPageRoute);
app.use(profilePageRoute);
app.use(productEditRoute);

app.post("/register", upload.single("icon"), async (req, res) => {
    try {
        const { name, email, password } = req.body;
        const image = req.file?.filename;
        const hashedPw = await hashPassword(password);
        if (!name) {
            res.status(401).json({
                message: "Please fill in name",
                success: false,
            });
            return;
        }
        if (!email) {
            res.status(401).json({
                message: "Please fill in email",
                success: false,
            });
            return;
        }
        if (!password) {
            res.status(401).json({
                message: "Please fill in password",
                success: false,
            });
            return;
        }

        if (image) {
            await client.query("insert into users(icon,username,email,password,created_at,updated_at) values ($1,$2,$3,$4,NOW(),NOW())", [image, name, email, hashedPw]);
        } else {
            await client.query("insert into users(username,email,password,created_at,updated_at) values ($1,$2,$3,NOW(),NOW())", [name, email, hashedPw]);
        }
        res.json({ message: "register success please login" });
        // res.redirect("/login.html");
    } catch (err) {
        console.log(err);
    }
});

app.post("/login", upload.none(), async (req, res) => {
    try {
        const userData = req.body;
        const { email, password } = userData;

        const result = await client.query("SELECT * FROM users WHERE email=$1", [email]);
        const match = result.rowCount > 0;

        const matchedUser = result.rows[0];
        let isPwValid = await checkPassword(password, matchedUser.password);
        if (match && isPwValid) {
            req.session["user"] = result.rows[0];
            res.json({
                message: "Login successfully",
                success: true,
                email: matchedUser,
            });
            
        } else {
            res.status(401).json({
                message: "Email or password is incorrect",
                success: false,
            });
        }
    } catch (error) {
        console.log(error);
        res.status(401).json({
            message: "Email or password is incorrect",
            success: false,
        });
    }
});

app.post("/logout", (req, res) => {
    try {
        req.session.destroy(() => {});
        res.json({ message: "Successfully logout" });
    } catch (error) {
        console.log(error);
        res.json({ message: "Fail to logout" });
    }
});

function createResultJSON(data: any | undefined = {}, err: any | undefined = {}) {
    return {
        data,
        err,
    };
}

app.get("/profile", (req, res) => {
    if (req.session && req.session["user"]) {
        res.json(createResultJSON({ user: req.session["user"] }));
        console.log(req.session);

        return;
    }

    res.status(400).json(createResultJSON(null, { message: "No login user" }));
});

app.get("/check-product", (req, res) => {
    if (req.session && req.session["product"]) {
        res.json(createResultJSON({ user: req.session["product"] }));
        console.log(req.session);

        return;
    }

    res.status(400).json(createResultJSON(null, { message: "No product" }));
});

const isLoggedIn = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.session && req.session["user"]) {
        next();
    } else {
        // res.json({ success: false, error: "User login first!" });
        res.redirect("/login.html");
    }
};

app.use(express.static("public"));
app.use(express.static("uploads"));
app.use("/admin", isLoggedIn, express.static("private"));

// app.use((req, res) => {
//   res.redirect("/404.html");
// });

const PORT = 8888;

server.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});
