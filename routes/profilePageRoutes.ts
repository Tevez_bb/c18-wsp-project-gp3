import express from "express";
import { client } from "../main";
const profilePageRoute = express.Router();
//campaignsCompleted
profilePageRoute.get("/campaignsCompleted", async (req, res) => {
    let campaignsCompletedId = req.session["user"].id;
    let campaignsCompletedResult = await client.query(`
  select p.name, p.price, u.username,p.id as product_id, p.image from group_purchases gp 
  inner join product p on p.id = gp.product_id 
  inner join users u on u.id = p.users_id 
  where gp.status like 'success'
  and gp.users_id = ${campaignsCompletedId}
order by gp.created_at  
    `);
    let campaignsCompletedData = campaignsCompletedResult.rows;
    res.json(campaignsCompletedData);
});
//campaignsRunning
profilePageRoute.get("/campaignsRunning", async (req, res) => {
    let campaignsRunningId = req.session["user"].id;
    let campaignsRunningResult = await client.query(`
  select p.name, p.price, p.id as product_id, p.image from product p where p.users_id =${campaignsRunningId} 
    `);
    let campaignsRunningDate = campaignsRunningResult.rows;
    res.json(campaignsRunningDate);
});
//campaignsJoined
profilePageRoute.get("/campaignsJoined", async (req, res) => {
    let campaignsJoinedId = req.session["user"].id;
    let campaignsJoinedResult = await client.query(`
  select p.name, p.price, u.username,p.id as product_id, p.image from join_purchase jp 
  inner join group_purchases gp on gp.id = jp.group_purchases_id 
  inner join product p on p.id = gp.product_id 
  inner join users u on u.id = p.users_id 
  where jp.users_id = ${campaignsJoinedId}
  order by jp.created_at  
    `);
    let campaignsJoinedDate = campaignsJoinedResult.rows;
    res.json(campaignsJoinedDate);
});

export default profilePageRoute;
