import express from "express";
import { client } from "../main";

const catalogRoute = express.Router();

catalogRoute.get("/all-product", async (req, res) => {
  let allCatalogResult = await client.query(`
    select p.name, p.price, u.username, p.image, p.id as product_id from product p
      inner join users u
          on u.id = p.users_id 
`);
  // console.log(productResult.rows);
  let allCatalogData = allCatalogResult.rows;

  res.json(allCatalogData);
});

catalogRoute.get("/all-product/:id", async (req, res) => {
  const catalogId = req.params.id;

  let catalogResult = await client.query(`
    select c.id, p.name, p.price, u.username, p.image, p.id as product_id, c.name as catagories_name from product p
      inner join users u
          on u.id = p.users_id 
      inner join group_purchases gp
          on gp.product_id = p.id
      inner join categories c
          on c.id = gp.categories_id
      where c.id = ${catalogId}`);
  let catalogTitle = await client.query(`
select c.name from categories c where c.id = ${catalogId}
      `);
      console.log(`catalogResult:`, catalogResult);
      
  console.log(`catalogResult.rows: `, catalogResult.rows);
  let catalogData = catalogResult.rows;
  let catalogTitleData = catalogTitle.rows;

  res.json({ catalogData, catalogTitleData });
});

export default catalogRoute;
