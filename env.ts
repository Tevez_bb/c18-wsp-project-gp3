import dotenv from "dotnev";

dotenv.config();

const env = {
    DB_NAME: process.env.DB_NAME || "teckbuy",
    DB_USER: process.env.DB_USERNAME || "teckbuy",
    DB_PASSWORD: process.env.DB_PASSWORD || "teckbuy",
};

export default env;
