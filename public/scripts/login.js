const loginForm = document.querySelector("#loginform");
loginForm.addEventListener("submit", async (e) => {
  e.preventDefault();
  const formData = new FormData(loginForm);
  console.log(formData);
  const res = await fetch("/login", {
    method: "POST",
    body: formData,
  });
  const parseRes = await res.json();
  if (parseRes.success === false) {
    document.querySelector(".error-msg").innerText = parseRes.message;
  } else {
    window.location.replace("/");
  }
  console.log("user:", parseRes);
});
