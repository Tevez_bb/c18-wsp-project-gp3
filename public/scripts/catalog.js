let params = new URLSearchParams(document.location.search);
let catalogId = params.get("catalogId");
console.log(catalogId);

async function fetchAllCatalog() {
  let res = await fetch("/all-product");
  let allCatalogs = await res.json();
  let catalogContainerElem = document.querySelector(".catalog-container");
  let catalogTitleElem = document.querySelector(".catagories-title");

  for (let allCatalog of allCatalogs) {
    let newAllCatalogsString = getAllCatalogsHTMLString(allCatalog);
    catalogContainerElem.innerHTML += newAllCatalogsString;
  }
  let newCatalogTitle = getTitle();
  catalogTitleElem.innerHTML += newCatalogTitle;

  function getTitle() {
    let titleHTMLString = `<div class="title">All Products</div>`;
    return titleHTMLString;
  }

  function getAllCatalogsHTMLString(allCatalog) {
    let catalogHTMLString = `
    <div class="col">
    <div class="card h-100 product-card">
    <a href="/product.html?productId=${allCatalog.product_id}">
        <img src="${allCatalog.image}" class="card-img-top my-img">
        </a>
        <div class="card-body">
            <h5 class="card-title">${allCatalog.name}</h5>
            <p class="card-text">${allCatalog.username}</p>
            <p class="price-text">${allCatalog.price}</p>
        </div>
    </div>
</div>
          `;
    return catalogHTMLString;
  }
  return allCatalogs.length;
}
if (catalogId == null) {
  fetchAllCatalog();
}

async function fetchCatalog() {
  console.log("fetchCatalog");
  let res = await fetch(`/all-product/${catalogId}`);
  let catalogs = await res.json();
  const catalogsList = catalogs.catalogData;
  const catalogsTitleList = catalogs.catalogTitleData;
  console.log("catalogs = ", catalogs);
  console.log("catalogsList = ", catalogsList);
  let catalogContainerElem = document.querySelector(".catalog-container");
  let catalogTitleElem = document.querySelector(".catagories-title");

  for (let catalogsLists of catalogsList) {
    let newCatalogsString = getCatalogsHTMLString(catalogsLists);
    catalogContainerElem.innerHTML += newCatalogsString;
  }
  for (let catalogsTitleLists of catalogsTitleList) {
    let newCatalogTitle = getTitle(catalogsTitleLists);
    catalogTitleElem.innerHTML += newCatalogTitle;
  }

  function getTitle(catalogsTitleLists) {
    let titleHTMLString = `<div class="title">${catalogsTitleLists.name}</div>`;
    return titleHTMLString;
  }

  function getCatalogsHTMLString(catalogsLists) {
    let catalogHTMLString = `
    <div class="col">
    <div class="card h-100 product-card">
    <a href="/product.html?productId=${catalogsLists.product_id}">
        <img src="${catalogsLists.image}" class="card-img-top my-img">
        </a>
        <div class="card-body">
            <h5 class="card-title">${catalogsLists.name}</h5>
            <p class="card-text">${catalogsLists.username}</p>
            <p class="price-text">${catalogsLists.price}</p>
        </div>
    </div>
</div>
          `;
    return catalogHTMLString;
  }

  // document.querySelector(".all-card").setAttribute("hidden", "");

  // document.querySelector(".all-title").setAttribute("hidden", "");
}

fetchCatalog();

async function getCategories() {
  const res = await fetch("/categories");
  const resObj = await res.json();
  const categoriesList = resObj.data;
  const categoriesSelect = document.querySelector(".list-group");
  for (let categoriesItem of categoriesList) {
    let opt = document.createElement("a");
    opt.className = "list-group-item list-group-item-action my-list";
    opt.href = `/catalog.html?catalogId=${categoriesItem.id}`;
    opt.innerHTML = categoriesItem.name;
    categoriesSelect.appendChild(opt);
  }
}

getCategories();
