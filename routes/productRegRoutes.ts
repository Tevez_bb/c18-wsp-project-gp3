import express from "express";
import { client } from "../main";
import multer from "multer";
import path from "path";
import { Request, Response } from "express";

const productRegRoutes = express.Router();

productRegRoutes.get("/categories", getAllCategories);

async function getAllCategories(req: Request, res: Response) {
  try {
    const categoriesList = (await client.query("select * from categories"))
      .rows;
    res.json({ data: categoriesList });
  } catch (error) {
    console.log(error);
    res.status(401).json({ success: false, data: error });
  }
}

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve("./uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
  },
});

export const upload = multer({ storage });

productRegRoutes.post(
  "/product-register",
  upload.single("image"),
  async (req, res) => {
    try {
      const { name, categories, qtn, expiry, description, district, price } =
        req.body;
        console.log(req.body)
      const image = req.file?.filename;
      console.log("req.session =", req.session["user"]);
      const userId = req.session["user"].id;
      console.log(`userId: `, userId);
      const productResult = await client.query(
        "insert into product(image,users_id,qtn,description,district,name,price,created_at,updated_at) values ($1,$2,$3,$4,$5,$6,$7,NOW(),NOW()) returning id",
        [image, userId, qtn, description, district, name, price]
      );
      const productId = productResult.rows[0].id;

      // (
      //   await client.query(
      //     `select id from categories where name = '${categories}'`
      //   )
      // ).rows[0].id;
      
      await client.query(
        "insert into group_purchases(users_id,expiry_date,product_id,categories_id,status,created_at,updated_at) values ($1,$2,$3,$4,'open',NOW(),NOW())",
        [userId, expiry, productId, categories]
      );
      res.json({ message: "register success" });
    } catch (err) {
      console.log(err);
    }
  }
);

export default productRegRoutes;
