import fetch from "node-fetch";
import crypto from "crypto";
import { hashPassword } from "../hash";
import express from "express";
import { client } from "../main";

const userRoutes = express.Router();
userRoutes.get("/login/google", loginGoogle);

async function loginGoogle(req: express.Request, res: express.Response) {
  const accessToken = req.session?.["grant"].response.access_token;

  const fetchRes = await fetch(
    "https://www.googleapis.com/oauth2/v2/userinfo",
    {
      method: "get",
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }
  );
  const result = await fetchRes.json();
  console.log(result);

  const users = (
    await client.query(`SELECT * FROM users WHERE email = $1`, [result.email])
  ).rows;
  console.log(`users`, users);  //{ id, icon, email, username, password}
  
  let user = users[0];
  if (!user) {
    let googlePW = await hashPassword(crypto.randomBytes(20).toString("hex"));
    let googleResult = await client.query(
      "Insert into users(icon,username,email, password, created_at, updated_at) values ($1,$2,$3,$4, NOW(), NOW()) returning *",
      [result.picture, result.name, result.email, googlePW]
    );
    user = googleResult.rows[0];
    // console.log(`googleResult`, googleResult);
    // console.log(`user`, user);
  }
  if (req.session && user) {
    req.session["user"] = user;
    console.log("req.session=", req.session);
  }
  return res.redirect("/");
}

export default userRoutes;
