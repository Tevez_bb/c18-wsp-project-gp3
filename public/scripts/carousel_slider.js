const socket = io.connect();

socket.on("new-product", (productData) => {
  const { fileName, productName, userName, productPrice } = productData;
  socketAddProduct(productData);
});

async function socketAddProduct(productData) {
  let productsContainerElem = document.querySelector(".products-container");
  let newProductsString = await getProductsHTMLString(productData);
  productsContainerElem.innerHTML =
    newProductsString + productsContainerElem.innerHTML.innerHTML;
}

async function fetchProducts() {
  let res = await fetch("/products");
  let products = await res.json();
  let productsContainerElem = document.querySelector(".products-container");

  // console.log(products);

  for (let product of products) {
    let newProductsString = getProductsHTMLString(product);
    productsContainerElem.innerHTML += newProductsString;
  }
  $(".products-container").owlCarousel({
    autoPlay: 3000,
    items: 6,
    itemsDesktop: [1199, 3],
    itemsDesktopSmall: [979, 3],
    center: true,
    nav: true,
    loop: true,
    responsive: {
      600: {
        items: 6,
      },
    },
  });
}
fetchProducts();

function getProductsHTMLString(productData) {
  // console.log(productData);
  let productsHTMLString = `
      <div class="item">
      <a class="hoverfx" href="/product.html?productId=${productData.productid}">
        <img src="${productData.image}">
      </a>
      <div class="carousel-product-name">${productData.name}</div>
      <div class="carousel-seller">${productData.username}</div>
      <div class="carousel-price">${productData.price}</div>
    </div>
      `;
  return productsHTMLString;
}
