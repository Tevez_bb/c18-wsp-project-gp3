let params = new URLSearchParams(document.location.search);
let productId = params.get("productId");
console.log(`productID: `, productId);

async function getCategories() {
    const res = await fetch("/categories");
    const resObj = await res.json();
    const categoriesList = resObj.data;
    const categoriesSelect = document.querySelector("#categoriesSelect");
    for (let categoriesItem of categoriesList) {
        let opt = document.createElement("option");
        opt.value = categoriesItem.id;
        opt.innerHTML = categoriesItem.name;
        categoriesSelect.appendChild(opt);
    }
}
getCategories();

async function fetchCampaignInfo() {
    // console.log("hello");
    let res = await fetch(`/campaign-detail/${productId}`);
    let campaignInfo = await res.json();
    //   console.log(`productDetailList:`, productDetailList);
    const allProductInfo = campaignInfo.campaignData;
    const productOwner = campaignInfo.isOwner;

    console.log(`allProductInfo: `, allProductInfo);
    console.log(`productOwner =`, productOwner);

    let campaignInfoContainerElem = document.querySelector("#campaign-info-container");
    let newCampaignString = getCampaignInfoHTMLString(allProductInfo);
    campaignInfoContainerElem.innerHTML = newCampaignString;
}
fetchCampaignInfo();

function getCampaignInfoHTMLString(allCampaignInfo) {
    let campaignInfoHTMLString = `
                <div class="mb-3">
                <label class="form-label">Product Name</label>
                <input type="text" class="form-control" id="exampleForm" value="${allCampaignInfo.name}" name="name">
            </div>
            <div class="mb-3">
                <label class="form-label">Product Categories</label>
                <select class="form-select" aria-label="Default select example" name="categories"
                id="categoriesSelect">
                <option selected>Categories</option>
                </select>   
            </div>
            <div class="mb-3">
                <label class="form-label">Upload a new product image</label>
                <input accept="image/*" class="form-control" type="file" id="imgInp" name="image">
                <img id="blah" src="#" alt="image preview" />
            </div>
            <div class="mb-3">
                <label class="form-label">No. of participants</label>
                <input type="number" class="form-control" id="exampleForm" value="${allCampaignInfo.qtn}" name="qtn">
            </div>
            <div class="mb-3">
                <label class="form-label">Campaign end date</label>
                <input type="date" class="form-control" id="exampleForm" value="${allCampaignInfo.expiry_date}" name="expiry">
            </div>
            <div class="mb-3">
                <label class="form-label">Product descriptions</label>
                <input type="text" class="form-control" id="exampleForm" value="${allCampaignInfo.description}" name="description">
            </div>
            <div class="mb-3">
                <label class="form-label">Your district</label>
                <input type="text" class="form-control" id="exampleForm" value="${allCampaignInfo.district}" name="district">
            </div>
            <div class="mb-3">
                <label class="form-label">Price</label>
                <input type="Number" class="form-control" id="exampleForm" value="${allCampaignInfo.price}" name="price">
            </div>
    `;
    return campaignInfoHTMLString;
}

//
const updateCampaignForm = document.querySelector("#campaign-form");
updateCampaignForm.addEventListener("submit", async (e) => {
    e.preventDefault();
    const formData = new FormData(updateCampaignForm);
    const res = await fetch(`/campaign-update/${productId}`, {
        method: "put",
        body: formData,
    });
    const parseRes = await res.json();
    if (res.ok) {
        console.log("updated");
    }
});

//

imgInp.onchange = (evt) => {
    const [file] = imgInp.files;
    if (file) {
        blah.src = URL.createObjectURL(file);
    }
};
