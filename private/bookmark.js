async function fetchUser() {
  let res = await fetch("/profile");
  if (res.ok) {
    let result = await res.json();
    let resultData = result.data;
    document.querySelector(".avatar").innerHTML = "";
    document.querySelector(".avatar").removeAttribute("hidden");
    document.querySelector(".btn-logout").removeAttribute("hidden");
    document.querySelector(".btn-login").setAttribute("hidden", "");

    // console.log(resultData.user.icon);

    if (resultData.user.icon) {
      document.querySelector(
        ".avatar"
      ).innerHTML += `<img src="${resultData.user.icon}" class="avatar-style"></img>`;
    } else if (resultData.user.icon == "null") {
      document.querySelector(
        ".avatar"
      ).innerHTML += `<img src="./default-avatar.jpeg" class="avatar-style"></img>`;
    }
  } else {
    document.querySelector(".btn-login").removeAttribute("hidden");
    document.querySelector(".avatar").setAttribute("hidden", "");
    document.querySelector(".btn-logout").setAttribute("hidden", "");
  }
}
fetchUser();

document.querySelector(".btn-logout").addEventListener("click", async (e) => {
  e.preventDefault();
  // let logoutElem = e.currentTarget;
  let res = await fetch("/logout", {
    method: "POST",
  });
  if (res.ok) {
    console.log("logged out");
    let result = await res.json();
    document.querySelector(".btn-login").removeAttribute("hidden");
    document.querySelector(".avatar").setAttribute("hidden", "");
    document.querySelector(".btn-logout").setAttribute("hidden", "");
  }
});
// document.querySelector(".btn-login").addEventListener("click", function () {
//   window.location.href = "login.html";
// });
// document
//   .querySelector(".btn-create-deal")
//   .addEventListener("click", function () {
//     window.location.href = "./product_reg.html";
//   });
document.querySelector(".header-logo").addEventListener("click", function () {
  window.location.href = "/";
});
////////////////////////////////////////////////////////////////////////////////////////////////

async function fetchAllCatalog() {
  let res = await fetch("/bookmark");
  let allCatalogs = await res.json();
  let catalogContainerElem = document.querySelector(".bookmark-container");

  for (let allCatalog of allCatalogs) {
    let newAllCatalogsString = getAllCatalogsHTMLString(allCatalog);
    catalogContainerElem.innerHTML += newAllCatalogsString;
  }

  function getAllCatalogsHTMLString(allCatalog) {
    let catalogHTMLString = `
    <div class="col">
    <div class="card h-100 product-card">
    <a href="/product.html?productId=${allCatalog.product_id}">
        <img src="/${allCatalog.image}" class="card-img-top">
        </a>
        <div class="card-body">
            <h5 class="card-title">${allCatalog.name}</h5>
            <p class="card-text">${allCatalog.username}</p>
            <p class="price-text">${allCatalog.price}</p>
        </div>
    </div>
</div>
          `;
    return catalogHTMLString;
  }
}

fetchAllCatalog();
