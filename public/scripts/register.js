const registerForm = document.querySelector("#register-form");
const registerResultElem = document.querySelector("#register-result");

registerForm.addEventListener("submit", async (e) => {
  e.preventDefault();
  const formData = new FormData(registerForm);
  const res = await fetch("/register", {
    method: "post",
    body: formData,
  });
  const parseRes = await res.json();

  if (!parseRes.success) {
    registerResultElem.innerText = parseRes.message;
    console.log("redirect la plz");
    window.location.replace = "/";
  } else {
    const email = e.target.email.value;
    const password = e.target.password.value;
    //   console.log(email,password)
    await fetch("/login", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    });
  }
});
