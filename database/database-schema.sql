﻿-- Exported from QuickDBD: https://www.quickdatabasediagrams.com/
-- Link to schema: https://app.quickdatabasediagrams.com/#/d/HBVVzU
-- NOTE! If you have used non-SQL datatypes in your design, you will have to change these here.
-- Modify this code to update the DB schema diagram.
-- To reset the sample schema, replace everything with
-- two dots ('..' - without quotes).
CREATE TABLE "users" (
    "id" SERIAL PRIMARY key,
    "icon" varchar(255),
    "username" varchar(255),
    "email" varchar(255),
    "password" varchar(255),
    "created_at" timestamp,
    "updated_at" timestamp
);
CREATE TABLE "bookmark" (
    "id" SERIAL PRIMARY key,
    "users_id" int,
    "group_purchases_id" int,
    "created_at" timestamp,
    "updated_at" timestamp
);
CREATE TABLE "product" (
    "id" SERIAL PRIMARY key,
    "users_id" int,
    "name" varchar(255),
    "image" varchar(255),
    "qtn" integer,
    "description" varchar(255),
    "district" varchar(255),
    "price" money,
    "created_at" timestamp,
    "updated_at" timestamp
);
CREATE TABLE "join_purchase" (
    "id" SERIAL PRIMARY key,
    "group_purchases_id" int,
    "users_id" int,
    "created_at" timestamp,
    "updated_at" timestamp
);
CREATE TABLE "group_purchases" (
    "id" SERIAL PRIMARY key,
    "product_id" int,
    "status" varchar(255),
    "users_id" int,
    "expiry_date" date,
    "categories_id" int,
    "created_at" timestamp,
    "updated_at" timestamp
);
CREATE TABLE "categories" (
    "id" SERIAL PRIMARY key,
    "name" varchar(255),
    "created_at" timestamp,
    "updated_at" timestamp
);
ALTER TABLE "bookmark"
ADD CONSTRAINT "fk_bookmark_users_id" FOREIGN KEY("users_id") REFERENCES "users" ("id");
ALTER TABLE "bookmark"
ADD CONSTRAINT "fk_bookmark_group_purchases_id" FOREIGN KEY("group_purchases_id") REFERENCES "group_purchases" ("id");
ALTER TABLE "product"
ADD CONSTRAINT "fk_product_users_id" FOREIGN KEY("users_id") REFERENCES "users" ("id");
ALTER TABLE "join_purchase"
ADD CONSTRAINT "fk_join_purchase_group_purchases_id" FOREIGN KEY("group_purchases_id") REFERENCES "group_purchases" ("id");
ALTER TABLE "join_purchase"
ADD CONSTRAINT "fk_join_purchase_users_id" FOREIGN KEY("users_id") REFERENCES "users" ("id");
ALTER TABLE "group_purchases"
ADD CONSTRAINT "fk_group_purchases_product_id" FOREIGN KEY("product_id") REFERENCES "product" ("id");
ALTER TABLE "group_purchases"
ADD CONSTRAINT "fk_group_purchases_users_id" FOREIGN KEY("users_id") REFERENCES "users" ("id");
ALTER TABLE "group_purchases"
ADD CONSTRAINT "fk_group_purchases_categories_id" FOREIGN KEY("categories_id") REFERENCES "categories" ("id");