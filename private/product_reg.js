const registerProductForm = document.querySelector("#product-form");

registerProductForm.addEventListener("submit", async (e) => {
  e.preventDefault();
  const formData = new FormData(registerProductForm);
  const res = await fetch("/product-register", {
    method: "post",
    body: formData,
  });
  const parseRes = await res.json();

  if (res.ok) {
    window.location = "/";
  }
});

imgInp.onchange = (evt) => { //preview
  const [file] = imgInp.files;
  if (file) {
    blah.src = URL.createObjectURL(file);
  }
};

async function getCategories() {
  const res = await fetch("/categories");
  const resObj = await res.json();
  const categoriesList = resObj.data;
  const categoriesSelect = document.querySelector("#categoriesSelect");
  for (let categoriesItem of categoriesList) {
    let opt = document.createElement("option");
    opt.value = categoriesItem.id;
    opt.innerHTML = categoriesItem.name;
    categoriesSelect.appendChild(opt);
  }
}
getCategories();

//
async function fetchUser() {
  let res = await fetch("/profile");
  if (res.ok) {
    let result = await res.json();
    let resultData = result.data;
    document.querySelector(".avatar").innerHTML = "";
    document.querySelector(".avatar").removeAttribute("hidden");
    document.querySelector(".btn-logout").removeAttribute("hidden");
    // document.querySelector(".btn-login").setAttribute("hidden", "");
    // console.log(resultData.user.icon);

    if (resultData.user.icon) {
      document.querySelector(
        ".avatar"
      ).innerHTML += `<img src="${resultData.user.icon}" class="avatar-style"></img>`;
    } else if (resultData.user.icon == null) {
      // console.log("print icon la");
      document.querySelector(
        ".avatar"
      ).innerHTML += `<img src="./assets/default-avatar.jpeg" class="avatar-style"></img>`;
    }
  } else {
    // document.querySelector(".btn-login").removeAttribute("hidden");
    document.querySelector(".avatar").setAttribute("hidden", "");
    // document.querySelector(".btn-logout").setAttribute("hidden", "");
  }
}
fetchUser(); 

// document.querySelector(".btn-login").addEventListener("click", function () {
//   window.location.href = "login.html";
// });
// document.querySelector(".btn-create-deal").addEventListener("click", function () {
//     window.location.href = "admin/product_reg.html";
//   });
// document.querySelector(".btn-logout").addEventListener("click", async (e) => {
//   e.preventDefault();
//   // let logoutElem = e.currentTarget;
//   let res = await fetch("/logout", {
//     method: "POST",
//   });
//   if (res.ok) {
//     console.log("logged out");
//     let result = await res.json();
//     document.querySelector(".btn-login").removeAttribute("hidden");
//     document.querySelector(".avatar").setAttribute("hidden", "");
//     document.querySelector(".btn-logout").setAttribute("hidden", "");
//   }
// });
document.querySelector(".header-logo").addEventListener("click", function () {
  window.location.href = "/";
});
