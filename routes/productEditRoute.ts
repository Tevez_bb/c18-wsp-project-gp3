import express from "express";
import { client } from "../main";
import multer from "multer";
import path from "path";
import { Request, Response } from "express";

const productEditRoute = express.Router();

productEditRoute.get("/categories", getAllCategories);

async function getAllCategories(req: Request, res: Response) {
    try {
        const categoriesList = (await client.query("select * from categories")).rows;
        res.json({ data: categoriesList });
    } catch (error) {
        console.log(error);
        res.status(401).json({ success: false, data: error });
    }
}

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./uploads"));
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
    },
});

export const upload = multer({ storage });

productEditRoute.put("/campaign-update/:id", upload.single("image"), async (req, res) => {
    const productId = req.params.id;
    console.log(`productID`, productId);
    try {
        const { name, categories, qtn, expiry, description, district, price } = req.body;
        if (false) {
            console.log({ name, categories, qtn, expiry, description, district, price });
        }

        const priceInt = Number(price);
        const qtnInt = Number(qtn);

        if (!qtnInt || !priceInt) {
            res.status(400).json({ msg: "invalid input " });
            return;
        }

        console.log(req.body);
        const image = req.file?.filename;
        console.log("req.session =", req.session["user"]);
        const userId = req.session["user"].id;
        console.log(`userId: `, userId);
        const campaignResult = await client.query(
            `UPDATE public.product
      SET "name"=$1, image=$2, qtn=$3, description=$4, district=$5, price=$6, created_at=NOW(), updated_at=NOW()
      WHERE id=  $7 returning id`,
            [name, image, qtnInt, description, district, priceInt, userId]
        );
        //   const campaignRes = campaignResult.rows[0];
        console.log(`campaignResult:`, campaignResult);

        // await client.query("UPDATE group_purchases(users_id,expiry_date,product_id,categories_id,status,created_at,updated_at) values ($1,$2,$3,$4,'open',NOW(),NOW())", [userId, expiry, productId, categories]);
        res.json({ message: "update success" });
    } catch (err) {
        console.log(err);
    }
});

//
productEditRoute.get("/campaign-detail/:id", async (req, res) => {
    const productId = req.params.id;
    console.log(`productID`, productId);

    let campaignResult = await client.query(`
    select u.id, p.name, p.price, gp.expiry_date, gp.categories_id as category_id, p.district, p.qtn, p.image, u.username, p.description, c.name as category from product p
      inner join users u
          on u.id = p.users_id 
      inner join group_purchases gp
          on gp.product_id = p.id
      inner join categories c
          on c.id = gp.categories_id
      where p.id = ${productId}`);
    // console.log(productResult.rows);
    let campaignData = campaignResult.rows[0];
    // console.log(`productResult:`, productResult);
    // console.log(`productData:`, productData);
    // console.log(`req`, req.session["user"].id);
    // console.log(`res`, productResult.rows[0].id);
    let isOwner = req.session["user"].id == campaignData.id ? true : false;
    // let isLoggedIn = req.session["user"]
    // console.log(`isOwner: `, isOwner);
    res.json({ campaignData, isOwner });
});

export default productEditRoute;
