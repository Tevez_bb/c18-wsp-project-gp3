import express from "express";
import { client } from "../main";
import { isLoggedInAPI } from "../middlewares/guard";
// import { Request, Response } from "express";
const productPageRoute = express.Router();

productPageRoute.get("/product-detail/:id", async (req, res) => {
    const productId = req.params.id;
    console.log(`productID`, productId);

    let productResult = await client.query(`
  select u.id, p.name, p.price, gp.expiry_date, gp.categories_id as category_id, p.district, p.qtn, p.image, u.username, p.description, c.name as category from product p
	inner join users u
		on u.id = p.users_id 
	inner join group_purchases gp
		on gp.product_id = p.id
	inner join categories c
		on c.id = gp.categories_id
    where p.id = ${productId}`);
    // console.log(productResult.rows);
    let productData = productResult.rows[0];
    // console.log(`productResult:`, productResult);
    // console.log(`productData:`, productData);

    let viewPoint = "guest";
    if (req.session && req.session["user"]) {
        viewPoint = req.session["user"].id == productData.id ? "owner" : "others";
    }

    // let isOwner = req.session["user"].id == productData.id ? true : false;
    // let isLoggedIn = req.session["user"]
    // console.log(`isOwner: `, isOwner);
    let categoryId = productData.category_id;
    console.log(categoryId);

    let gpIdRows = await client.query(`
  select id from group_purchases gp where product_id =${productId}`);
    let gpId = gpIdRows.rows[0].id;
    let joinCountResult = await client.query(`select count (*) from join_purchase jp where group_purchases_id = ${gpId}`);
    let joinCountData = joinCountResult.rows;

    let relatedItemResult = await client.query(`
  select u.id, p.name, p.price, gp.categories_id, p.id as productid, p.image, u.username, c.name as category from product p
	inner join users u
		on u.id = p.users_id 
	inner join group_purchases gp
		on gp.product_id = p.id
	inner join categories c
		on c.id = gp.categories_id
    where gp.categories_id = ${categoryId}
  `);
    let relatedItemData = relatedItemResult.rows;
    console.log(`relatedItemData: `, relatedItemData);
    // res.json({ productData, relatedItemData, joinCountData });
    res.json({ productData, viewPoint, relatedItemData, joinCountData });
});

productPageRoute.get("/join/:id", isLoggedInAPI, async (req, res) => {
    const joinId = req.params.id;
    let gpIdRows = await client.query(`
  select id from group_purchases gp where product_id =${joinId}`);
    let gpId = gpIdRows.rows[0].id;
    let userId = req.session["user"].id;
    if (!userId) {
        res.status(400).json({ msg: "login first" });
        return;
    }
    if (userId) {
        //isLoggedIn
        const hasJoined = (await client.query("SELECT * FROM join_purchase WHERE users_id = $1 AND group_purchases_id = $2", [userId, gpId])).rowCount > 0;
        let JoinedOrNotStatus = hasJoined ? true : false;
        res.json({
            status: JoinedOrNotStatus,
        });
    }
});

productPageRoute.put("/join/:id", async (req, res) => {
    const joinId = req.params.id;

    let gpIdRows = await client.query(`
  select id from group_purchases gp where product_id =${joinId}`);
    let gpId = gpIdRows.rows[0].id;
    let userId = req.session["user"].id;
    try {
        if (userId) {
            //isLoggedIn
            const hasJoined = (await client.query("SELECT * FROM join_purchase WHERE users_id = $1 AND group_purchases_id = $2", [userId, gpId])).rowCount > 0;

            if (hasJoined) {
                await client.query("DELETE FROM join_purchase WHERE users_id = $1 AND group_purchases_id = $2", [userId, gpId]);
                res.json({
                    success: true,
                    message: "Unjoined campaign",
                    type: "remove",
                });
                return;
            } else {
                await client.query("INSERT INTO join_purchase (users_id,group_purchases_id,created_at,updated_at) VALUES ($1, $2,NOW(),NOW())", [userId, gpId]);
                res.json({
                    success: true,
                    message: "Joined campaign",
                    type: "add",
                });
                return;
            }
        } else {
            res.json({ success: false, message: "Please login" });
            return;
        }
    } catch (error) {
        console.log(error);
        res.json({ success: false, message: "Fail to join" });
    }
});

productPageRoute.get("/bookmark/:id", isLoggedInAPI, async (req, res) => {
    const pageId = req.params.id;
    let gpIdRows = await client.query(`
  select id from group_purchases gp where product_id =${pageId}`);
    let gpId = gpIdRows.rows[0].id;
    let userId = req.session["user"].id;
    if (!userId) {
        res.status(400).json({ msg: "login first" });
        return;
    }
    if (userId) {
        //isLoggedIn
        const hasBookmarked = (await client.query("SELECT * FROM bookmark WHERE users_id = $1 AND group_purchases_id = $2", [userId, gpId])).rowCount > 0;
        let BookmarkedOrNotStatus = hasBookmarked ? true : false;
        res.json({
            status: BookmarkedOrNotStatus,
        });
        return;
    }
});

productPageRoute.put("/bookmark/:id", async (req, res) => {
    const pageId = req.params.id;

    let gpIdRows = await client.query(`
  select id from group_purchases gp where product_id =${pageId}`);
    let gpId = gpIdRows.rows[0].id;
    let userId = req.session["user"].id;
    try {
        if (userId) {
            const hasJoined = (await client.query("SELECT * FROM bookmark WHERE users_id = $1 AND group_purchases_id = $2", [userId, gpId])).rowCount > 0;
            if (hasJoined) {
                await client.query("DELETE FROM bookmark WHERE users_id = $1 AND group_purchases_id = $2", [userId, gpId]);
                res.json({
                    success: true,
                    message: "Item removed from your wishlist.",
                    type: "remove",
                });
                return;
            } else {
                await client.query("INSERT INTO bookmark (users_id,group_purchases_id,created_at,updated_at) VALUES ($1, $2,NOW(),NOW())", [userId, gpId]);
                res.json({
                    success: true,
                    message: "Item added to your wishlist!",
                    type: "add",
                });
                return;
            }
        } else {
            res.json({ success: false, message: "Please login" });
            return;
        }
    } catch (error) {
        console.log(error);
        res.json({ success: false, message: "Fail to toggle bookmark" });
    }
});

export default productPageRoute;
