async function fetchUser() {
  let res = await fetch("/profile");
  if (res.ok) {
    let result = await res.json();
    let resultData = result.data;
    document.querySelector(".avatar").innerHTML = "";
    document.querySelector(".avatar").removeAttribute("hidden");
    document.querySelector(".btn-logout").removeAttribute("hidden");
    document.querySelector(".btn-login").setAttribute("hidden", "");

    // console.log(resultData.user.icon);

    if (resultData.user.icon) {
      document.querySelector(
        ".avatar"
      ).innerHTML += `<img src="${resultData.user.icon}" class="avatar-style"></img>`;
    } else if (resultData.user.icon == "null") {
      document.querySelector(
        ".avatar"
      ).innerHTML += `<img src="./assets/default-avatar.jpeg" class="avatar-style"></img>`;
    }
  } else {
    document.querySelector(".btn-login").removeAttribute("hidden");
    document.querySelector(".avatar").setAttribute("hidden", "");
    document.querySelector(".btn-logout").setAttribute("hidden", "");
  }
}
fetchUser();

document.querySelector(".btn-logout").addEventListener("click", async (e) => {
  e.preventDefault();
  // let logoutElem = e.currentTarget;
  let res = await fetch("/logout", {
    method: "POST",
  });
  if (res.ok) {
    console.log("logged out");
    let result = await res.json();
    document.querySelector(".btn-login").removeAttribute("hidden");
    document.querySelector(".avatar").setAttribute("hidden", "");
    document.querySelector(".btn-logout").setAttribute("hidden", "");
  }
});
document.querySelector(".btn-login").addEventListener("click", function () {
  window.location.href = "login.html";
});
// document
//   .querySelector(".btn-create-deal")
//   .addEventListener("click", function () {
//     window.location.href = "product_reg.html";
//   });
document.querySelector(".header-logo").addEventListener("click", function () {
  window.location.href = "/";
});
////////////////////////////////////////////////////////////////////////////////////////////////
// let htmlString = `
// <div class="col">
//     <div class="card h-100">
//         <img src="./teck-buyer-logo-128x44.png" class="card-img-top" alt="...">
//         <div class="card-body">
//             <h1>Nothing created</h1>
//         </div>
//     </div>
// </div>
// `;

//campaignsCompleted
async function campaignsCompleted() {
  let res = await fetch("/campaignsCompleted");
  let allCampaignsCompleteds = await res.json();
  let campaignsCompletedElem = document.querySelector(
    ".campaignsCompleted-container"
  );
  for (let allCampaignsCompleted of allCampaignsCompleteds) {
    let campaignsCompletedString = getAllCampaignsCompletedHTMLString(
      allCampaignsCompleted
    );
    campaignsCompletedElem.innerHTML += campaignsCompletedString;
  }
  function getAllCampaignsCompletedHTMLString(allCampaignsCompleted) {
    let allCompletedHTMLString = `
    <div class="col">
    <div class="card h-100 product-card">
    <a href="/product.html?productId=${allCampaignsCompleted.product_id}">
        <img src="/${allCampaignsCompleted.image}" class="card-img-top my-img">
        </a>
        <div class="card-body">
            <h5 class="card-title">${allCampaignsCompleted.name}</h5>
            <p class="card-text">${allCampaignsCompleted.username}</p>
            <p class="price-text">${allCampaignsCompleted.price}</p>
        </div>
    </div>
</div>
    `;
    return allCompletedHTMLString;
  }
}
campaignsCompleted();

//campaignsRunning
async function campaignsRunning() {
  let res = await fetch("/campaignsRunning");
  let allCampaignsRunnings = await res.json();
  console.log(allCampaignsRunnings);
  let campaignsRunningElem = document.querySelector(
    ".campaignsRunning-container"
  );
  ///////////////////
  //   campaignsRunningElem.innerHTML = htmlString;
  ///////////////////
  for (let allCampaignsRunning of allCampaignsRunnings) {
    let campaignsRunningString =
      getAllCampaignsRunningHTMLString(allCampaignsRunning);
    // campaignsRunningElem += campaignsRunningString;
    campaignsRunningElem.innerHTML += campaignsRunningString;
  }
  function getAllCampaignsRunningHTMLString(allCampaignsRunning) {
    let allRunningHTMLString = `
<div class="col">
<div class="card h-100 product-card">
<a href="/product.html?productId=${allCampaignsRunning.product_id}">
    <img src="/${allCampaignsRunning.image}" class="card-img-top my-img">
    </a>
    <div class="card-body">
        <h5 class="card-title">${allCampaignsRunning.name}</h5>
        <p class="price-text">${allCampaignsRunning.price}</p>
    </div>
</div>
</div>
          `;
    return allRunningHTMLString;
  }
}
campaignsRunning();

//campaignsJoined
async function campaignsJoined() {
  let res = await fetch("/campaignsJoined");
  let allCampaignsJoineds = await res.json();
  let campaignsJoinedElem = document.querySelector(
    ".campaignsJoined-container"
  );
  for (let allCampaignsJoined of allCampaignsJoineds) {
    let campaignsJoinedString =
      getAllCampaignsJoinedHTMLString(allCampaignsJoined);
    // campaignsJoinedElem += campaignsJoinedString;
    campaignsJoinedElem.innerHTML += campaignsJoinedString;
  }
  function getAllCampaignsJoinedHTMLString(allCampaignsJoined) {
    let allJoinedHTMLString = `
    <div class="col">
<div class="card h-100 product-card">
<a href="/product.html?productId=${allCampaignsJoined.product_id}">
    <img src="/${allCampaignsJoined.image}" class="card-img-top my-img">
    </a>
    <div class="card-body">
        <h5 class="card-title">${allCampaignsJoined.name}</h5>
        <p class="card-text">${allCampaignsJoined.username}</p>
        <p class="price-text">${allCampaignsJoined.price}</p>
    </div>
</div>
</div>
    `;
    return allJoinedHTMLString;
  }
}
campaignsJoined();

async function getUserInfo() {
  let res = await fetch("/profile");
  let profileLists = await res.json();
  let profileData = profileLists.data.user;
  let searchTerm = "https";
  // const indexOfFirst = profileData.icon.indexOf(searchTerm);

  if (profileData.icon == null) {
    // console.log("print icon la");
    document.querySelector(".user-box").innerHTML += `<div class="row">
    <div class="col-lg-1 avatarImg">
        <img class="userIcon" src="./big-avatar.png" alt="">
    </div>
    <div class="col-lg-5">
    <h2>${profileData.username}</h2>
    <h5>${profileData.email}</h5>
    
    </div>
    </div>`;
  } else if (profileData.icon.indexOf(searchTerm) == 0) {
    document.querySelector(".user-box").innerHTML += `<div class="row">
    <div class="col-lg-1 avatarImg">
        <img class="userIcon" src="${profileData.icon}" alt="">
    </div>
    <div class="col-lg-5">
        <h2>${profileData.username}</h2>
        <h5>${profileData.email}</h5>
    
    </div>
    </div>`;
  } else if (profileData.icon) {
    document.querySelector(".user-box").innerHTML += `<div class="row">
    <div class="col-lg-1 avatarImg">
        <img class="userIcon" src="/${profileData.icon}" alt="">
    </div>
    <div class="col-lg-5">
        <h2>${profileData.username}</h2>
        <h5>${profileData.email}</h5>
    
    </div>
    </div>`;
  }
}

getUserInfo();
