let socket = io();

let form = document.getElementById("form");
let messages = document.getElementById("messages");
let input = document.getElementById("input");

form.addEventListener("submit", function (e) {
    e.preventDefault();
    if (input.value) {
        // socket.emit("chat message", input.value);

        fetch("/message", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ message: input.value }),
        });
        input.value = "";
    }
});

// let onlineUser = await fetch("/onlineUser");
let onlineUser = "username";

socket.emit("join-chatroom", 99);

socket.on("chat message", function (message) {
    console.log("msg from io server : ", message);
    let item = document.createElement("li");
    item.innerHTML = `${message.from} : ${message.content}`;
    // item.innerHTML =
    //   '<img src="https://source.unsplash.com/B2mq60Ksrsg/800x800" alt="" width="20px" height="20px">' +
    //   msg;

    messages.appendChild(item);
    window.scrollTo(0, document.body.scrollHeight);
});

// let socket = io();

// let form = document.getElementById("form");
// let messages = document.getElementById("messages");
// let input = document.getElementById("input");

// form.addEventListener("submit", function (e) {
//   e.preventDefault();
//   if (input.value) {
//     socket.emit("chat message", input.value);
//     input.value = "";
//   }
// });

// socket.on("chat message", function (msg) {
//   let item = document.createElement("li");
//   item.textContent = msg;
//   messages.appendChild(item);
//   window.scrollTo(0, document.body.scrollHeight);
// });
